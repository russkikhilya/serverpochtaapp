using Microsoft.EntityFrameworkCore;
using ServerPochtaApp.Model;
using System;

namespace ServerPochtaApp.Context
{
  /// <summary>
  /// Контекст для работы с БД
  /// </summary>
  public class MessageContext : DbContext
  {
    public DbSet<Record> Records { get; set; }

    public MessageContext()
    {
      try
      {
        Database.EnsureCreated();
      }
      catch(Exception ex)
      {
        Console.WriteLine($"Failed to connect database. {ex}");
      }
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      var connectionStrings = $"Host={Configuration.DatabaseConnectionOptions.Host};Port={Configuration.DatabaseConnectionOptions.Port};" +
        $"Database={Configuration.DatabaseConnectionOptions.DatabaseName};Username={Configuration.DatabaseConnectionOptions.UserName};Password={Configuration.DatabaseConnectionOptions.Password}";
      optionsBuilder.UseNpgsql(connectionStrings);
    }
  }
}
