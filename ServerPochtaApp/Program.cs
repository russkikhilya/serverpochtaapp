using ServerPochtaApp.Utils;
using System;

namespace ServerPochtaApp
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.SetWindowSize(200, 50);
      var printCommand = "-print";
      Console.WriteLine("Hello, this is messages subscriber!");
      Console.WriteLine("Enter command -print for print all messages");
      var rabbitService = new RabbitSubscribeService();
      rabbitService.Subscribe();
      while (true)
      {
        var message = Console.ReadLine();
        if (printCommand.Equals(message.ToLower()))
        {
          Operations.PrintAllMessages();
        }
      }
    }
  }
}
