namespace ServerPochtaApp
{
  /// <summary>
  /// Сервис для работы с rabbit
  /// </summary>
  public interface IRabbitSubscribeService
  {
    void Subscribe();
  }
}
