using ServerPochtaApp.Model;
using System;
using System.Linq;

namespace ServerPochtaApp.Utils
{
  /// <summary>
  /// Операции
  /// </summary>
  public class Operations
  {
    /// <summary>
    /// Вывести все записи из БД
    /// </summary>
    public static void PrintAllMessages()
    {
      var records = DbHelper.GetAllRecords();
      if (!records.Any())
      {
        Console.WriteLine("Messages not yet");
        return;
      }
      Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------");
      Console.WriteLine($"|{nameof(Record.Id), -10}|  {nameof(Record.Message), -50}|  {nameof(Record.IPClient), -30}|  {nameof(Record.ReceivedDate), -20}|");
      foreach (var record in records)
      {
        Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------");
        Console.WriteLine($"|{record.Id, -10}|  {record.Message, -50}|  {record.IPClient, -30}|  {record.ReceivedDate, -20}|");
      }
      Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------");
    }
  }
}
