using ServerPochtaApp.Context;
using ServerPochtaApp.Model;
using System.Collections.Generic;
using System.Linq;

namespace ServerPochtaApp.Utils
{
  /// <summary>
  /// Помощник для БД
  /// </summary>
  public class DbHelper
  {
    /// <summary>
    /// Добавить запись в БД
    /// </summary>
    /// <param name="record"></param>
    public static void InsertRecord(Record record)
    {
      using (var context = new MessageContext())
      {
        context.Records.Add(record);
        context.SaveChanges();
      }
    }

    /// <summary>
    /// Получить все записи из БД
    /// </summary>
    public static IEnumerable<Record> GetAllRecords()
    {
      using (var context = new MessageContext())
      {
        return context.Records.ToList();
      }
    }
  }
}
