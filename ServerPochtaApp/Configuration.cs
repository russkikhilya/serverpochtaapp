using Microsoft.Extensions.Configuration;
using ServerPochtaApp.Model;
using System.IO;

namespace ServerPochtaApp
{
  public static class Configuration
  {
    private static IConfigurationRoot _configuration;

    public static RabbitConnectionOptions RabbitConnectionOptions { get; }

    public static RabbitParams RabbitParams { get; }

    public static DatabaseConnectionOptions DatabaseConnectionOptions { get; }

    static Configuration()
    {
      _configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
        .Build();
      var connectionSection = GetSectionConf(nameof(RabbitConnectionOptions));
      RabbitConnectionOptions = new RabbitConnectionOptions()
      {
        HostName = connectionSection["Hostname"],
        Password = connectionSection["Password"],
        UserName = connectionSection["Username"]
      };

      var rabbitParamsSection = GetSectionConf(nameof(RabbitParams));
      RabbitParams = new RabbitParams()
      {
        ExchangeName = rabbitParamsSection["Exchange"],
        QueueName = rabbitParamsSection["Queue"],
        RoutingKey = rabbitParamsSection["Routingkey"]
      };

      var databaseConnectionSection = GetSectionConf("ConnectionStrings");
      DatabaseConnectionOptions = new DatabaseConnectionOptions()
      {
        DatabaseName = databaseConnectionSection["Database"],
        Host = databaseConnectionSection["Host"],
        Password = databaseConnectionSection["Password"],
        Port = databaseConnectionSection["Port"],
        UserName = databaseConnectionSection["Username"]
      };
    }

    /// <summary>
    /// Получить блок конфигурации по имени
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private static IConfigurationSection GetSectionConf(string name)
    {
      return _configuration.GetSection(name);
    }
  }
}
