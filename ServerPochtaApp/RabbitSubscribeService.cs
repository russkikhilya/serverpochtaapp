using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServerPochtaApp.Model;
using ServerPochtaApp.Utils;
using System;
using System.Collections.Concurrent;
using System.Text.Json;

namespace ServerPochtaApp
{
  public class RabbitSubscribeService : IRabbitSubscribeService
  {
    private readonly ConnectionFactory _factory;
    private readonly ConcurrentDictionary<string, bool> _declaredExchanges;
    private readonly ConcurrentDictionary<string, bool> _declaredQueue;
    private readonly string queueName;
    private readonly string exchangeName;
    private readonly string routingKey;

    public RabbitSubscribeService()
    {
      _factory = new ConnectionFactory()
      {
        UserName = Configuration.RabbitConnectionOptions.UserName,
        Password = Configuration.RabbitConnectionOptions.Password,
        HostName = Configuration.RabbitConnectionOptions.HostName,
        Port = AmqpTcpEndpoint.UseDefaultPort,
        VirtualHost = "/",
      };

      _declaredExchanges = new ConcurrentDictionary<string, bool>();
      _declaredQueue = new ConcurrentDictionary<string, bool>();

      exchangeName = Configuration.RabbitParams.ExchangeName;
      queueName = Configuration.RabbitParams.QueueName;
      routingKey = Configuration.RabbitParams.RoutingKey;
    }

    /// <summary>
    /// Подписаться на прослушку сообщений
    /// </summary>
    public void Subscribe()
    {
      var channel = CreateChannel();

      DeclareExchange(exchangeName, channel);
      DeclareQueue(queueName, channel);
      channel.QueueBind(queueName, exchangeName, routingKey);

      var consumer = new EventingBasicConsumer(channel);
      consumer.Received += (sender, eventArgs) => OnMessageReceived((EventingBasicConsumer)sender, eventArgs);

      channel.BasicConsume(
          queue: queueName,
          autoAck: false,
          consumer: consumer,
          exclusive: false);
    }

    /// <summary>
    /// Действие на получение сообщения
    /// </summary>
    /// <param name="consumer"></param>
    /// <param name="eventArgs"></param>
    private void OnMessageReceived(EventingBasicConsumer consumer, BasicDeliverEventArgs eventArgs)
    {
      Console.WriteLine("Received new message");
      var receivedMessage = JsonSerializer.Deserialize<ReceivedMessage>(eventArgs.Body.Span);
      DbHelper.InsertRecord(new Record() { IPClient = receivedMessage.IPClient, Message = receivedMessage.Message, ReceivedDate = DateTime.Now });
      consumer.Model.BasicAck(eventArgs.DeliveryTag, false);
    }

    /// <summary>
    /// Создание канала для rabbit
    /// </summary>
    /// <returns></returns>
    private IModel CreateChannel()
    {
      var connection = _factory.CreateConnection();
      return connection.CreateModel();
    }

    /// <summary>
    /// Объявление обменника для rabbit
    /// </summary>
    /// <param name="exchangeName"></param>
    /// <param name="channel"></param>
    private void DeclareExchange(string exchangeName, IModel channel)
    {
      _declaredExchanges.GetOrAdd(exchangeName, exchange =>
      {
        channel.ExchangeDeclare(exchangeName, ExchangeType.Topic, durable: true);
        return true;
      });
    }

    /// <summary>
    /// Объявление очереди для rabbit
    /// </summary>
    /// <param name="queueName"></param>
    /// <param name="channel"></param>
    private void DeclareQueue(string queueName, IModel channel)
    {
      _declaredQueue.GetOrAdd(queueName, exchange =>
      {
        channel.QueueDeclare(queueName, durable: true, autoDelete: false, exclusive: false);
        return true;
      });
    }
  }
}
