using System;

namespace ServerPochtaApp.Model
{
  /// <summary>
  /// Запись в БД
  /// </summary>
  public class Record
  {
    /// <summary>
    /// Ид записи
    /// </summary>
    public int Id { get; set; }  
    /// <summary>
    /// Сообщение
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    /// Ip клиента
    /// </summary>
    public string IPClient { get; set; }

    /// <summary>
    /// Время получения
    /// </summary>
    public DateTime ReceivedDate { get; set; }
  }
}
