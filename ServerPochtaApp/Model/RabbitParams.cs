namespace ServerPochtaApp.Model
{
  /// <summary>
  /// Параметры rabbit
  /// </summary>
  public class RabbitParams
  {
    /// <summary>
    /// Имя обменника
    /// </summary>
    public string ExchangeName { get; set; }

    /// <summary>
    /// Имя очереди
    /// </summary>
    public string QueueName { get; set; }

    /// <summary>
    /// Имя ключа
    /// </summary>
    public string RoutingKey { get; set; }
  }
}
