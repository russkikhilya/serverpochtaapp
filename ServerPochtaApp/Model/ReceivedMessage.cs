namespace ServerPochtaApp.Model
{
  /// <summary>
  /// Полученное сообщение из rabbit
  /// </summary>
  public class ReceivedMessage
  {
    /// <summary>
    /// Сообщение
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    /// Ip клиента
    /// </summary>
    public string IPClient { get; set; }
  }
}
