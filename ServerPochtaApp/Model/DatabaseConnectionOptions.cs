namespace ServerPochtaApp.Model
{
  /// <summary>
  /// Настройки подлкючения к БД
  /// </summary>
  public class DatabaseConnectionOptions
  {
    /// <summary>
    /// Хост
    /// </summary>
    public string Host { get; set; }

    /// <summary>
    /// Порт
    /// </summary>
    public string Port { get; set; }

    /// <summary>
    /// Имя БД
    /// </summary>
    public string DatabaseName { get; set; }

    /// <summary>
    /// Имя пользователя
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// Пароль пользователя
    /// </summary>
    public string Password { get; set; }
  }
}
